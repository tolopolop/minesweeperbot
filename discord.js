const discord = require("discord.js");
const express = require("express");
const fs = require("fs");
const app = express();
const minesweeper = require("./minesweeper.js");

const port = 3000

const Client = new discord.Client();

let userGames = {};

Client.on("ready", function () {
  console.log("Bot is ready!");
});

Client.on("message", function (message) {
  if (!userGames[message.channel.id]) {
    userGames[message.channel.id] = {};
  }

  let args = message.content.match(/(?:[^\s"]+|"[^"]*")+/g) || [];

  if (args[0] == "<@" + Client.user.id + ">") {
    if (process.argv[2] == "-d") {
      console.log(new Date() + ": " + args.join(" ") + " from " + message.guild.name + " with " + [...message.guild.members].length + " members.");
    }

    message.reply({
      embed: {
        title: "Help",
        description: "Go to [https://minesweeperbot.glitch.me/commands](https://minesweeperbot.glitch.me/commands) for all commands"
    }});
  }

  if (args[0] == "!minesweeper") {
    if (process.argv[2] == "-d") {
      console.log(new Date() + ": " + args.join(" ") + " from " + message.guild.name + " with " + [...message.guild.members].length + " members.");
    }

    if (userGames[message.channel.id] && userGames[message.channel.id][message.member.user.id]) {
      userGames[message.channel.id][message.member.user.id].commandmsg.delete().catch(() => {});
      userGames[message.channel.id][message.member.user.id].replymsg.delete().catch(() => {})

      delete userGames[message.channel.id][message.member.user.id];
    }

    if (args[1] && args[1] == "-p") {
      !args[2] && (args[2] = 8);
      let width = parseInt(args[1]) || 8;

      !args[3] && (args[3] = args[2]);
      let height = parseInt(args[3]) || 8;

      !args[4] && (args[4] = (args[3] * args[2]) / 5);
      let mines = parseInt(args[4]) || 14;

      let grid = minesweeper(width, height, mines);

      /*message.reply({
        embed: {
          title: "Minesweeper :boom:",
          color: 0x7CFC00,
          description: "```" + grid + "```",
          footer: {
            icon_url: message.member.user.avatarURL,
            text: message.member.user.username + "#" + message.member.user.discriminator
          }
      }})*/
      message.reply("```" + grid + "```").then(m => {
        userGames[message.channel.id][message.member.user.id] = {
          commandmsg: message,
          replymsg: m,
          channel: message.channel,
          minesweeper: {
            width,
            height,
            mines
          }
        };
      }).catch(e => {
        message.reply("Error: Board size is too big!");
      });
    } else {
      !args[1] && (args[1] = 8);
      let width = parseInt(args[1]) || 8;

      !args[2] && (args[2] = args[1]);
      let height = parseInt(args[2]) || 8;

      !args[3] && (args[3] = (args[2] * args[1]) / 5);
      let mines = parseInt(args[3]) || 14;

      let grid = minesweeper(width, height, mines);

      /*message.reply({
        embed: {
          title: "Minesweeper :boom:",
          color: 0x7CFC00,
          description: "\n" + grid,
          footer: {
            icon_url: message.member.user.avatarURL,
            text: message.member.user.username + "#" + message.member.user.discriminator
          }
      }})*/
      message.reply("\n" + grid).then(m => {
        userGames[message.channel.id][message.member.user.id] = {
          commandmsg: message,
          replymsg: m,
          channel: message.channel,
          minesweeper: {
            width,
            height,
            mines
          }
        };
      }).catch(e => {
        message.reply("Error: Board size is too big!");
      });
    }
  }
  if (args[0] == "!restart") {
    if (process.argv[2] == "-d") {
      console.log(new Date() + ": " + args.join(" ") + " from " + message.guild.name + " with " + [...message.guild.members].length + " members.");
    }

    if (userGames[message.channel.id] && userGames[message.channel.id][message.member.user.id]) {
      userGames[message.channel.id][message.member.user.id].commandmsg.delete().catch(() => {});
      userGames[message.channel.id][message.member.user.id].replymsg.delete().catch(() => {});

      let { width, height, mines } = userGames[message.channel.id][message.member.user.id];

      let grid = minesweeper(width, height, mines);

      message.reply("\n" + grid).then(m => {
        userGames[message.channel.id][message.member.user.id] = {
          commandmsg: message,
          replymsg: m,
          channel: message.channel,
          minesweeper: {
            width,
            height,
            mines
          }
        }
      }).catch(e => {
        message.reply("Error: Board size is too big!");
      });
    }
  }
  if (args[0] == "!end") {
    if (process.argv[2] == "-d") {
      console.log(new Date() + ": " + args.join(" ") + " from " + message.guild.name + " with " + [...message.guild.members].length + " members.");
    }

    if (!message.member) return;

    if (userGames[message.channel.id] && userGames[message.channel.id][message.member.user.id]) {
      userGames[message.channel.id][message.member.user.id].commandmsg.delete().catch(() => {});
      userGames[message.channel.id][message.member.user.id].replymsg.delete().catch(() => {});

      userGames[message.channel.id][message.member.user.id].commandmsg.reply("It took you " +
        ((message.createdTimestamp - userGames[message.channel.id][message.member.user.id].commandmsg.createdTimestamp) / 1000) +
        "s to complete the minesweeper (" + userGames[message.channel.id][message.member.user.id].minesweeper.width + "," + userGames[message.channel.id][message.member.user.id].minesweeper.height + "," + userGames[message.channel.id][message.member.user.id].minesweeper.mines + ")");

      delete userGames[message.channel.id][message.member.user.id];
    }

    message.delete().catch(() => {});
  }
  if (args[0] == "!endall") {
    if (process.argv[2] == "-d") {
      console.log(new Date() + ": " + args.join(" ") + " from " + message.guild.name + " with " + [...message.guild.members].length + " members.");
    }

    /*if (message.member.hasPermission("MANAGE_MESSAGES")) {
      let k = Object.keys(userGames);

      for (let i in k) {
        userGames[k[i]].commandmsg.delete().catch(() => {});
        userGames[k[i]].replymsg.delete().catch(() => {});

        delete userGames[message.member.user.id];
      }
    }

    message.delete();*/
  }
  if (args[0] == "!help") {
    if (process.argv[2] == "-d") {
      console.log(new Date() + ": " + args.join(" ") + " from " + message.guild.name + " with " + [...message.guild.members].length + " members.");
    }

    message.reply({
      embed: {
        title: "Help",
        description: "Go to [https://minesweeperbot.glitch.me/commands](https://minesweeperbot.glitch.me/commands) for all commands"
    }});
  }
  if (args[0] == "!servercount") {
    if (process.argv[2] == "-d") {
      console.log(new Date() + ": " + args.join(" ") + " from " + message.guild.name + " with " + [...message.guild.members].length + " members.");
    }

    message.reply("Server count: " + [...Client.guilds.keys()].length);

    let b = "";

    Client.guilds.forEach(c => {
      b += c.name + " " + [...c.members.keys()].length + "\n";
    });
  }
});

Client.login(process.env.TOKEN || fs.readFileSync(__dirname + "/token.txt").toString().trim());

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/index.html");
});

app.get("/commands", function (req, res) {
  res.sendFile(__dirname + "/commands.html");
});

app.use("/res/", express.static(__dirname + "/res/"));

app.listen(port);

module.exports = Client;
