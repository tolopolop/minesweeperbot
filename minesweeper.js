function genMineSweeper(gridX = 8, gridY = 8, mines = 14) {
  if (gridX.constructor != Number) gridX = 8;
  if (gridY.constructor != Number) gridY = 8;
  if (mines.constructor != Number) mines = 14;

  if (gridX * gridY <= mines * 2) {
   return new Error("Too many mines!");
  }

  function genGridMatrix(x = gridX, y = gridY, fill = 0) {
    let out_ = [];

    while (y--) {
      out_.push((new Array(x)).fill(fill));
    }

    return out_;
  }

  function getSurroundingInMatrix(arr, x, y) {
    let i = 0;

    if (arr[y - 1] && arr[y - 1][x - 1] == "|| :boom: ||") i++;
    if (arr[y - 1] && arr[y - 1][x] == "|| :boom: ||") i++;
    if (arr[y - 1] && arr[y - 1][x + 1] == "|| :boom: ||") i++;
    if (arr[y] && arr[y][x - 1] == "|| :boom: ||") i++;
    if (arr[y] && arr[y][x + 1] == "|| :boom: ||") i++;
    if (arr[y + 1] && arr[y + 1][x - 1] == "|| :boom: ||") i++;
    if (arr[y + 1] && arr[y + 1][x] == "|| :boom: ||") i++;
    if (arr[y + 1] && arr[y + 1][x + 1] == "|| :boom: ||") i++;

    let e = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight"];

    return "|| :" + e[i] + ": ||";
  }

  function genMines(arr, x, y, mines_) {
    while (mines_--) {
     let ranX = Math.floor(Math.random() * x);
     let ranY = Math.floor(Math.random() * y);

     while (arr[ranY][ranX] == "|| :boom: ||") {
       ranX = Math.floor(Math.random() * x);
       ranY = Math.floor(Math.random() * y);
     }

     arr[ranY][ranX] = "|| :boom: ||";
    }

    return arr;
  }

  let matrix = genGridMatrix();
  matrix = genMines(matrix, gridX, gridY, mines);
  matrix = matrix.map((x1, y) => {
    return x1.map((x2, x) => {
      if (x2 != "|| :boom: ||") {
        return getSurroundingInMatrix(matrix, x, y);
      } else {
        return x2;
      }
    });
  });

  let allZeros = [];

  for (let y in matrix) {
    for (let x in matrix[y]) {
      if (matrix[y][x] == "|| :zero: ||") {
        allZeros.push({
          x: x,
          y: y
        });
      }
    }
  }

  let ran = allZeros[Math.floor(Math.random() * allZeros.length)];

  if (ran && matrix[ran.y] && matrix[ran.y][ran.x]) matrix[ran.y][ran.x] = " :zero: ";

  let fullGrid = matrix.map(c => {
      return c.join("");
  }).join("\n");

  /*.replace(/:[^ \n:]+:/g, c => {
      return "|| " + c + " ||";
  });*/

  return fullGrid;
}

module.exports = genMineSweeper;
